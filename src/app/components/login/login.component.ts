import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services/common.service';
import { PublicService } from 'src/app/services/public.service';
import { RestServiceConstants } from 'src/app/constants/rest-service-constants';
import { ApiResponse } from 'src/app/models/api-response';
import { Router } from '@angular/router';
import { MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  
  selected = new FormControl(0);
  constructor(private commonService: CommonService, private publicService: PublicService,
    private router: Router) { }

  ngOnInit(): void {
  }

  registerForm = new FormGroup({
    name : new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z ]*$')]),
    emailAddress : new FormControl('', [Validators.required, Validators.email]),
    countryCode : new FormControl(''),
    mobileNumber : new FormControl('', [Validators.required, Validators.pattern('^[0-9]*$')]),
//    password : new FormControl('',  [Validators.required, Validators.pattern('^(?=.{6,})(?=.*[@#$%^&+=]).*$')]),
    password : new FormControl('',  [Validators.required]),
    confirmPassword : new FormControl('', [Validators.required])
  });

  loginForm = new FormGroup({
    emailAddress : new FormControl('', [Validators.required]),
    password : new FormControl('', [Validators.required]),
  });

  register(): void{

    if (this.registerForm.valid){
     
      this.publicService.register(this.registerForm).subscribe((result: ApiResponse) => {
        if(result.code === RestServiceConstants.USER_REGISTERED){
            let message = 'User registered';
            this.commonService.displaySnackBarMessages(message, 3000);
            this.selected.setValue(1);
            this.registerForm.reset();
            this.router.navigate(['home']);
        }else{
            let message = 'User not registered';
            this.commonService.displaySnackBarMessages(message, 3000);
        }
      });
    }else{
        let message = 'Invalid registration form';
        this.commonService.displaySnackBarMessages(message, 3000);
    }

  }

  login(): void{
    if (this.loginForm.valid){
      this.publicService.login(this.loginForm).subscribe((result: ApiResponse) => {
        if(result.code === RestServiceConstants.USER_AUTHENTICATION_VALID){
          const data = result.data as any;
          localStorage.setItem('X_AUTH_TOKEN', data.X_AUTH_TOKEN);
          this.publicService.publishAuthSubject('logged-in');
          this.router.navigate(['home']);
        }else{
          let message = 'Invalid login';
          this.commonService.displaySnackBarMessages(message, 3000);
        }
      });
    }
  }
  
}
