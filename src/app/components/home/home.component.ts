import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { RestServiceConstants } from 'src/app/constants/rest-service-constants';
import { ApiResponse } from 'src/app/models/api-response';
import { PublicService } from 'src/app/services/public.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isLoggedIn: boolean = false;
  dataSource:  MatTableDataSource<any>;
  displayedColumns: string[] ;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private publicService: PublicService, private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    
    this.displayedColumns = [ 'name', 'emailAddress', 'mobileNumber'];
    this.isLoggedIn = this.publicService.isLoggedIn();
    if(!this.isLoggedIn){
      this.router.navigate(['login']);
    }

    this.userService.getAllUsers().subscribe((result: ApiResponse) => {
      if(result.code === RestServiceConstants.USER_LIST_FOUND){
        const data = result.data as any;
        this.dataSource = new MatTableDataSource(data.userList);
        this.dataSource.sortingDataAccessor = (obj, property) => this.getProperty(obj, property);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate = (data: any, filter) => {
          const dataStr =JSON.stringify(data).toLowerCase();
          return dataStr.indexOf(filter) != -1;
        }
      }
    });
    
  }

  getProperty = (obj: any, path: string) => (
    path.split('.').reduce((o, p) => o && o[p], obj)
  )

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
