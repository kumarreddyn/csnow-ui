import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicService } from 'src/app/services/public.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn: boolean = false;

  constructor(private publicService: PublicService, private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn = this.publicService.isLoggedIn();
    this.publicService.subscribeAuthSubject().subscribe(result => {
      this.isLoggedIn = this.publicService.isLoggedIn();
    });
  }

  logout(): void{
    localStorage.clear();
    this.publicService.publishAuthSubject('logged-out');
    this.router.navigate(['login']);
  }

  getPhotoURL(): string{    
    return '';  
  }

}
