import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  servicesURL: string;
  constructor(private http: HttpClient, private commonService: CommonService) {
    this.servicesURL = this.commonService.getServicesURL();
   }

  getAllUsers(): any {
    return this.http.get(this.servicesURL + '/user/get-all-users');
  }

}
