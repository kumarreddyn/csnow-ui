import { DOCUMENT } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private servicesURL: string;

  constructor(@Inject(DOCUMENT) private document: Document, private snackBar: MatSnackBar) { 
    this.servicesURL = document.location.protocol +'//'+ document.location.hostname + ':'+environment.servicesPort;
  }

  getServicesURL(): string{
    return this.servicesURL;
  }

  getJSONHeaders(): HttpHeaders {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return headers;
  }

  getEmptyHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    return headers;
  }

  displaySnackBarMessages(message: string, displayDuration: number): void{
    this.snackBar.open(message, '', {
      duration: displayDuration,
      horizontalPosition: 'right',
      verticalPosition: 'top',
    });
  }

}
