export class RestServiceConstants {
    
    public static USER_AUTHENTICATION_VALID = 'USER_AUTHENTICATION_VALID';
    public static USER_REGISTERED = 'USER_REGISTERED';

    public static USER_LIST_FOUND = 'USER_LIST_FOUND';
    
}
